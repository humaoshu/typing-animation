import TypingAnimation from "./src/main.vue";

TypingAnimation.install = Vue => {
  Vue.component(TypingAnimation.name, TypingAnimation);
};
export default TypingAnimation;
