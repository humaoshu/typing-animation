# vue-type-animation

模拟打字动画插件，基于 vue2.6，css 高度可定制化。

### Installation

#### NPM

```
npm i type-animation
```

#vue 全局使用

```js
import Vue from "vue";
import App from "./App.vue";
import TypingAnimation from "./../packages/index";

Vue.use(TypingAnimation);
Vue.config.productionTip = false;

new Vue({
  render: h => h(App)
}).$mount("#app");
```

```html
<typing-animation
  :text="
        '动次打次动次打次动次打次动次打次动次打次，动次打次动次打次动次打次动次打次动，次打次动次打次动次打次'
      "
  :type="'text'"
></typing-animation>
<h3>
  效果演示：命令行模式
</h3>
<typing-animation
  :text="
        '动次打次动次打次动次打次动次打次动次打次，动次打次动次打次动次打次动次打次动，次打次动次打次动次打次'
      "
  :type="'cmd'"
></typing-animation>
<h3>效果演示：单行模式</h3>
<typing-animation
  :text="
        '动次打次动次打次动次打次动次打次动次打次，动次打次动次打次动次打次动次打次动，次打次动次打次动次打次'
      "
  :type="'cmd'"
  :isSingle="true"
></typing-animation>
```

```js
<script>
export default {
  name: "HelloWorld",
  data() {
    return {
      isSingle: false,
      type: "cmd",
      isShow: true
    };
  },

  methods: {
    changeModel(model) {
      switch (model) {
        case "single":
          this.isSingle = !this.isSingle;
          this.isShow = !this.isShow;
          break;
        case "cmd":
          this.type = "cmd";
          this.isShow = !this.isShow;
          break;
        case "text":
          this.type = "text";
          this.isShow = !this.isShow;
          break;
        default:
          break;
      }
    }
  }
};
</script>
```

---

| 属性                       |  类型   |         默认值 |
| -------------------------- | :-----: | -------------: |
| type(样式类型)             | String  | text(cmd 可选) |
| separators(文本换行分隔符) | String  |           “，” |
| isSingle(是否开启单行模式) | Boolean |          false |
| text(显示的文本)           | String  |     （自定义） |

---

### Example

gitee 预览 [example](http://humaoshu.gitee.io/typing-animation/) 案例效果预览.

See `/examples` for some simple examples.

### Gitee，码云仓库

https://gitee.com/humaoshu/typing-animation/

master 为主分支，deploy 分支为 gitee pages 服务部署分支，配置及目录略有差异

### License

[MIT](http://opensource.org/licenses/MIT)
