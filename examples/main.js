import Vue from "vue";
import App from "./App.vue";
import TypingAnimation from "./../packages/index";

Vue.use(TypingAnimation);
Vue.config.productionTip = false;

new Vue({
  render: h => h(App)
}).$mount("#app");
